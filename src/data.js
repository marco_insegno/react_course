/**
 * @typedef Person
 * @type {Object}
 * @property {string} id - an ID.
 * @property {string} name - nome
 */

/** @type {[Person]}*/
export const data = [
  { id: 1, name: "omar" },
  { id: 2, name: "tazio" },
  { id: 3, name: "gianluca" },
  { id: 4, name: "anna" },
];

/**
 * @typedef Mobile
 * @type {Object}
 * @property {string} id - an ID.
 * @property {object} image - nome
 * @property {string} name - nome
 * @property {number} prezzo - prezzo
 */

/** @type {[Mobile]} */
export const datiIncompleti = [
  {
    id: "attcvDDMikF6G2iNi",
    image: {
      url:
        "https://img.freepik.com/free-vector/illustration-gallery-icon_53876-27002.jpg?w=1060&t=st=1682844637~exp=1682845237~hmac=d23101951d2daa2d5557b63aebc2513dbcdc3d5847facc2d2c4644a43c31cb25",
    },
    nome: "divano bello",
    prezzo: 50.45,
  },
  {
    id: "attP2cUyxU35M1zbw",
    image: {
      url:
        "https://img.freepik.com/free-vector/illustration-gallery-icon_53876-27002.jpg?w=1060&t=st=1682844637~exp=1682845237~hmac=d23101951d2daa2d5557b63aebc2513dbcdc3d5847facc2d2c4644a43c31cb25",
    },
    name: "cosa carina",
    prezzo: 70.45,
  },
  {
    id: "attIieysyB9zQmQo9",
    image: {
      url:
        "https://img.freepik.com/free-vector/illustration-gallery-icon_53876-27002.jpg?w=1060&t=st=1682844637~exp=1682845237~hmac=d23101951d2daa2d5557b63aebc2513dbcdc3d5847facc2d2c4644a43c31cb25",
    },
    nome: "cosa brutta",
    prezzo: 99.99,
  },
  {
    id: "attcvDDMikF6G2ifi",
    nome: "divano bello",
  },
];
