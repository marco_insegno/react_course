import Inizio from "./lezioni/14-useTransition/inizio/index"

function App() {
  return (
    <div className="App">
      <section className="container text-center my-3">
        <Inizio />
      </section>
    </div>
  );
}

export default App;
