import React from "react";
import Modal from "./Modal";
import { useState, useReducer } from 'react'
import reducer from './reducer'

const Index = () => {

  // const [modalState, setModalState] = useState(false)
  // const [modalContent, setModalContent] = useState('Ciao Sono una Modale')

  // const switchModal = () => {

  //   setModalState(!modalState)

  // }

  // return (
  //   <>
  //     <button className="btn btn-danger" onClick={switchModal}>Apri Modale</button>

  //     <Modal modalState={modalState} modalContent={modalContent} switchModal={switchModal}/>
  //   </>
  // );




  // ************************
  // ************************
  // VERSIONE CON USE-REDUCE()
  // ************************
  // ************************

  // la reducer function è esterna ed importata

  const initialState = {
    modalState: false,
    modalContent: 'Ciao Sono una Modale'
  }

  // la dispatch è una funzione che delega ad una funzione esterna, la reducer function,  il compito di modificare il nostro state.
  // Il compito della dispatch è quello di dare un descrizione di ciò che dev'esser gestito dalla reducer
  const [state, dispatch] = useReducer(reducer, initialState)

  const tiPremo = () => {
    dispatch(
      {
        type: 'PREMO_BOTTONE'
      }
    )
  }

  // possiamo passare dei parametri al reducer con PAYLOAD
  const openModal = () => {
    dispatch({
      type: 'OPEN_MODAL',
      payload: 'Il mio nuovo modal'
    })
  }

  const closeModal = () => {
    dispatch({
      type: 'CLOSE_MODAL'
    })
  }



  return (
    <>
      <button className="btn btn-danger" onClick={openModal}>Apri Modale</button>
      <Modal
        modalState={state.modalState}
        modalContent={state.modalContent}
        closeModal={closeModal}
      />
    </>
  );
};

export default Index;
