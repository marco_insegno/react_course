import React from 'react'
import '../../../index.css'

function Modal({ modalState, modalContent, closeModal}) {

    return (
        <section className=
            {`modal-section
            ${modalState
                    ? 'show-modal'
                    : ''}`
            }>
            <div className="container modal-content">
                <h4>
                    {
                        modalContent || 'Sono una Modale'
                    }
                </h4>
                <button className='btn btn-danger w-50 mx-auto mt-5' onClick={closeModal}>x</button>
            </div>

        </section>
    )
}

export default Modal