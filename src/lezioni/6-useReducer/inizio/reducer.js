// dalla reducer function dobbiamo SEMPRE returnare lo state
const reducer = (state, action) => {

    if (action.type === 'PREMO_BOTTONE') {
        
    }
    if (action.type === 'OPEN_MODAL') {
        
        return (
            {
                ...state,
                modalState: true,
                modalContent:action.payload
            }
        )
    }

    if (action.type === 'CLOSE_MODAL') {
        return (
            {
                ...state,
                modalState: false
            }
        )
    }
    return state
}

export default reducer