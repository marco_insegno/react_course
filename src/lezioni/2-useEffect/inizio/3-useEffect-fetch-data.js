import React from "react";
import axios from "axios";
import { useState, useEffect } from 'react'

const url = "https://api.github.com/users";

const FetchComponent = () => {

  const [users, setUsers] = useState([])

  const getData = async () => {
      const response = await axios.get(url)
      console.log(response);
      setUsers(response.data)
  }

  useEffect(() => {

    getData()
    // fetch(`${url}`)
    // .then((response) => response.json())
    // .then((data) => setUsers(data))
  },[])

  console.log(users);

  return (
    <>
      <h1>Fetch Component</h1>
      <ul>
        {users.map((user) => {
          const {login, id, avatar_url: img, html_url: url} = user
          return (
            <li key={id} className="shadow py-5">
              <img src={img} alt={login} title={`Hi, I'm ${login.charAt(0).toUpperCase()+login.slice(1)}`} style={{width:'100px'}} className="mb-3 img-fluid rounded-circle"/>
              <h5 className="text-capitalize">{login}</h5>
              <a href={url} target="_blank" rel="noreferrer">Visit Profile</a>
            </li>
          )
        })}
      </ul>
    </>
  );
};

export default FetchComponent;
