import React from "react";
import { useState, useEffect } from "react";

const useEffectBasi = () => {

  const [value, setValue] = useState(0);

  const add = () => {
    setValue((oldValue) => oldValue + 1)
  }

  // lo useEffect viene chiamato ogni volta che abbiamo un render, ma DOPO che il componente viene renderizzato
  // ogni volta che manipolerò il mio state causerò un nuovo render e avrò una chiamata allo useEffect 
  useEffect(() => {

    console.log('useEffect chiamata');

    // se dobbiamo fare una condizione SEMPRE all'interno MAI fuori
    if (value < 1) {
      document.title = `Nessun Messaggio`
    } else {
      document.title = `Arrivati: ${value} messaggi`
    }

    // clean-up function -> funzione che viene definita ad ogni esecuzione dello useEffect e lanciata prima del successivo render
    return () => {
      console.log('clean-up');
    }

  }, [value])

  return (
    <div>
      <h4 className="item">{value}</h4>
      <button className="btn btn-primary" onClick={add}>+</button>
    </div>
  );
};

export default useEffectBasi;
