import React from "react";
import { useState, useEffect } from 'react'

const CleanUp = () => {

  const [sizeWidth, setSizeWidth] = useState(window.innerWidth)

  const [sizeHeight, setSizeHeight] = useState(window.innerHeight)

  console.log('render');

  function windowWidth(){

    setSizeWidth(window.innerWidth)

  }

  useEffect(() => {

    console.log('setto addEventListener');

    window.addEventListener('resize', windowWidth)

    // funzione clean-up
    return () => {
      console.log('elimino addEventListener');
      window.removeEventListener('resize',windowWidth)
    }

  }, [sizeWidth])

  useEffect(() => {
    window.addEventListener('resize', () => {

      setSizeHeight(window.innerHeight);

    })
  })

  return (
    <div className="container w-75 col-6 py-4 shadow">
      <h1 className="display-1">{sizeWidth} x {sizeHeight}</h1>
      <h4>Dimensioni Schermo</h4>
    </div>
  );
};

export default CleanUp;
