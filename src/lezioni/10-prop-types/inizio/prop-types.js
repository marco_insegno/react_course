import React from "react";
import { datiIncompleti } from "../../../data";
// importo libreria per gestire i PropTypes già presente in React 
import PropTypes from "prop-types";

const defaultImage = 'https://cdn-icons-png.flaticon.com/512/16/16410.png?w=1380&t=st=1682845711~exp=1682846311~hmac=14c2823ef8b37d88f077f4caa0f86accbf530d74c7518bf778ca0ca2db71a9d7'


const PropComponent = () => {
  return (
    <div>
      {
        datiIncompleti && datiIncompleti.map((el) => {

          console.log(el);
          return (
            <Prodotto key={el.id} {...el} />
          )
        })
      }

    </div>
  );
};

const Prodotto = ({ nome, prezzo, image }) => {

  // salvo in costante il controllo se l'oggetto prodotto ha immagine
  const img = image && image.url

  return (
    <div className="border mb-2">
      {/* se ha un immagine applica quella passata altrimenti applica quella di default */}
      <img
        src={img || defaultImage}
        alt={nome || 'Prodotto default'}
        width='100%' />
      <h3>{nome || 'Prodotto default'}</h3>
      <h4>{prezzo || 10}€</h4>
    </div>
  )

}

// Prodotto.defaultProps = {
//   nome: 'Nome default',
//   prezzo: 10,
// }

Prodotto.propTypes = {
  nome: PropTypes.string.isRequired,
  prezzo: PropTypes.number.isRequired,
  image: PropTypes.object.isRequired,
}
export default PropComponent;
