import React from "react";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect } from "react";
import Card from 'react-bootstrap/Card';

const ControlledInput = () => {

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [number, setNumber] = useState('')
  const [people, setPeople] = useState([])


  const handleSubmit = (e) => {
    e.preventDefault()

    if (name && email && number) {

      setPeople(
        [
          ...people,
          {
            id: new Date(Date.now()).getTime().toString(),
            name: name,
            email: email,
            number: number
          }
        ]
      )
      setName('')
      setEmail('')
      setNumber('')
    } else {
      alert('inserisci tutti i campi del Form')
    }

  }


  useEffect(() => {

  }, [])


  return (
    <section className="container">
      <div className="row my-5">
        <div className="col-12">
          <h2>Iscriviti!!</h2>
        </div>
      </div>
      <div className="row justify-content-center">
        <div className="col-12 col-md-6 border p-5">
          <Form className="text-start" onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Name</Form.Label>
              <Form.Control type="text" placeholder="Enter name..." value={name} onChange={(e) => setName(e.target.value)} />

            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" placeholder="@..." value={email} onChange={(e) => setEmail(e.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Number</Form.Label>
              <Form.Control type="tel" placeholder="+()..." value={number} onChange={(e) => setNumber(e.target.value)} />
            </Form.Group>
            <Button variant="primary" type="submit" className="mt-4 d-flex">
              Iscriviti
            </Button>
          </Form>

        </div>
      </div>

      {
        people.length === 0 &&
        <div className="row my-5">
          <div className="col-12">
            <h2>Nessun iscritto...</h2>
          </div>
        </div>
      }

      {
        people.length > 0 &&
        <>
          <div className="row my-5">
            <div className="col-12">
              <h2>Si sono Iscritti...</h2>
            </div>
          </div>
          <div className="row">
            {
              people && people.map((el) => {

                return (
                  <div className="col-12 col-md-4 mb-2">
                    <Card bg='warning' text="dark" className="mb-2">
                      <Card.Body>
                        <Card.Title>{el.name}</Card.Title>
                        <Card.Text>{el.email}</Card.Text>
                        <Card.Text>{el.number}</Card.Text>
                      </Card.Body>
                    </Card>
                  </div>
                )
              })
            }

          </div>
        </>

      }

    </section>

  );
};

export default ControlledInput;
