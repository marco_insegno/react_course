import useFetch from "./useFetch";
const url = "https://jsonplaceholder.typicode.com/users";
const postUrl = "https://jsonplaceholder.typicode.com/posts";

const FetchComponents = () => {

  const {data, isLoading} = useFetch(url)

  return (
    <div>
      {isLoading ? (
        <h3>Loading....</h3>
      ) : ( 
        <div>
          {data.map((el) => {
            const { id, name, phone } = el;
            return (
              <div key={id} className="users bg-white p-4 shadow rounded">
                <h3>{name}</h3>
                <h5>{phone}</h5>
              </div>
            );
          })}
          <h1 className="border border-2 border-danger p-3 text-danger text-uppercase">Sezione Post</h1>
          <Post/>
        </div>
      )}
    </div>
  );
};

const Post = () =>{

  const {data, isLoading} = useFetch(postUrl)

  return (
    <div>
      {isLoading ? (
        <h3>Loading....</h3>
      ) : ( 
        <div>
          {data.map((el) => {
            const { id, title, body} = el;
            return (
              <div key={id} className="users bg-white p-4 shadow rounded d-flex flex-column">
                <h3><span className="fw-bolder">Titolo: </span>{title}</h3>
                <h5><span className="fw-bolder">Corpo: </span>{body}</h5>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );

}

export default FetchComponents;
