import React from 'react'
import { useParams } from 'react-router-dom'

function Profile1() {

    const  {id} = useParams()


    return (
        <section className="container mt-5">
            <div className="row">
                <div className="col-12">
                    <h2>Profilo con parametro {id}</h2>
                </div>
            </div>
        </section>
    )
}

export default Profile1