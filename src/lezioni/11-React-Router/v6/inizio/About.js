import React from 'react'
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';

function About() {

    const navigate = useNavigate();
    return (
        <section className="container mt-5">
            <div className="row">
                <div className="col-12">
                    <h2>Pagina About</h2>
                    {/* naviga due pagine indietro */}
                    <Button variant="danger"className='mt-5' onClick={()=>navigate(-2)}>Naviga 2 pagine indietro</Button>
                </div>
            </div>
        </section>
    )
}

export default About