import React from 'react'
import Navigation from './Navigation'
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Home from './Home';
import About from './About';
import Profile from './Profile';
import Error404 from './Error404';
import SingleProfile from './SingleProfile';
import MyProfile from './MyProfile';


function ReactRouterApp() {
    return (
        <>
            <Router>

                <Navigation />
                <Routes>
                    <Route path='/' element={<Home />} />
                    <Route path='/about' element={<About />} />
                    {/* Route nested */}
                    {/* Il componente che ha la ruote Parent deve avere importanto al suo interno il componente Outlet */}
                    <Route path='/profile' element={<Profile />} >
                        <Route path='/profile/:id' element={<SingleProfile />} />
                        <Route path='/profile/me' element={<MyProfile />} />
                    </Route>
                    <Route path='*' element={<Error404 />} />
                </Routes>

            </Router>
        </>
    )
}

export default ReactRouterApp