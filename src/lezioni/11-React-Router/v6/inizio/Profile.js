import React from 'react'
import { Outlet } from 'react-router-dom'

function Profile() {
    return (
        <section className="container mt-5">
            <div className="row">
                <div className="col-12">
                    <h2>Pagina Profilo</h2>
                </div>
            </div>
            {/* in Outlet andranno mostrati i componenti delle Route children in base al loro url*/}
            <Outlet/>
        </section>
    )
}

export default Profile