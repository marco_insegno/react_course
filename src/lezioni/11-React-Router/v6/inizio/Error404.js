import React from 'react'
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';

function Error404() {

  const navigate = useNavigate()

  return (
    <section className="container mt-5">
            <div className="row">
                <div className="col-12">
                    <h2>404 Page not found!!</h2>
                    <Button variant="danger"className='mt-5' onClick={()=>navigate('/')}>Torna alla Home</Button>

                </div>
            </div>
        </section>
  )
}

export default Error404