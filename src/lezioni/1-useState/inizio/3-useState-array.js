import React from "react";
import { data } from '../../../data.js';
import { useState } from 'react';


const ArrayState = () => {

  const [people, setPeople] = useState(data)

  const removeItem = (id) => {
    let newPeople = people.filter((el) => el.id !== id)

    setPeople(newPeople)

  }



  return (
    <>
      {
        people.map((el) => {

          const { id, name } = el

          return (
            <div key={id} className="item shadow">

              <h3 > {name}</h3>

              <button className="btn delete-button" onClick={()=>removeItem(id)}>X</button>
            </div>
          )
        }
        )
      }
    </>
  );
};

export default ArrayState;
