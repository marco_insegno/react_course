import React, { useState } from "react";

// Gli Hooks devono essere usasti con use davanti
// componenti devono avere la prima lettere Maiuscola
// Gli Hook devono essere invocati all'interno del corpo/funzione del componente
// Gli Hook non possono essere utilizzati in maniera condizionale

const UsoBase = () => {

  //   const value = useState()[0];
  //   const handler = useState()[1];
  //   console.log(value, handler);

  const [title, setTitle] = useState('Hello World!');

  const changeTitle = () => {

    if (title === 'Hello World!') {

      setTitle('Hello New World!!')

    } else {

      setTitle('Hello World!')

    }

  }


  return (
    <>
      <h2> {title}</h2>
      <button className="btn btn-info" onClick={changeTitle}>Change Title</button>
    </>
  );
};

export default UsoBase;
