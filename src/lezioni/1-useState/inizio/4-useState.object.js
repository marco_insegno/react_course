import React, { useState } from "react";

const StateObject = () => {

  const [person, setPerson] = useState(
    {
      name: 'Marco',
      age: 38,
      saluto: `Hello, I'm Marco`
    }
  )

  const changeAge = () => {

    setPerson(
      {
        ...person,
        age: 39,
        saluto: `Hello, I'm Insegno Marco`
      }
    )
  }


  return (

    <div className="item shadow">
      <div className="text-left">
        <h5>{person.name}</h5>
        <h5>{person.age}</h5>
        <h5>{person.saluto}</h5>
      </div>
      <button className="button" onClick={changeAge}>Change Object</button>

    </div>
  )
};

export default StateObject;
