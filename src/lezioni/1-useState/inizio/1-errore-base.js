import React from "react";
//Far vedere che il componente non si riaggiorna ma la variabile si
//Spiegare che cosa è un React Fragment e che è possibile abbreviarlo


const ErroreBase = () => {

  let title = 'Hello World'

  const changeTitle = () =>{

    title = 'New Hello World!'

    console.log(title);
    
  }


  return (
    <>
      <h2>{title}</h2>
      <button className="btn btn-primary" onClick={changeTitle}>Change Title</button>

    </>)
};

export default ErroreBase;
