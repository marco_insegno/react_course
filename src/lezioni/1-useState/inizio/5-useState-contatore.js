import React, { useState } from "react";

const CounterComponent = () => {

  const [contatore, setContatore] = useState(0);

  const reset = () => {
    setContatore(0);
  }

  const dim = () => {
    setContatore(contatore - 1);
  }

  const add = () => {
    setContatore((contatore) => {
      return contatore + 1;
    })
  }

  const addTwoSeconds = () => {

    setTimeout(function () {

      setContatore((oldValue) => {
        return oldValue + 1
      });

    }, 2000)

  }


  return (
    <div className="bg-white shadow rounded py-5 w-75 col-6 offset-3">
      <h3>{contatore}</h3>
      <div className="d-flex justify-content-between">
        <button className="button my-2" onClick={dim}>Diminuisci</button>
        <button className="button my-2" onClick={add}>Aumenta</button>
        <button className="button my-2" onClick={addTwoSeconds}>Aumenta dopo 2s</button>
      </div>
      <button className="button reset-button my-2" onClick={reset}>Reset</button>
    </div>
  );
};

export default CounterComponent;
