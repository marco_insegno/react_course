import React, { useCallback, useMemo, useState } from "react";
import useFetch from "../../9-custom-hooks/risultato/useFetch";
const url = "https://api.github.com/users";

// React.memo -> controlla aggiornamento dello state delle nostre props
// useMemo() -> memorizza un valore specifico e solo quando si ha una variazione di un valore effettua operazioni

const trovaMaggiore = (array) =>{
  return array.reduce((total,item) => {
    if(item.id > total){
      total = item.id
    }
    console.log(item);
    return total
  },0)
}

const Index = () => {
  const { data } = useFetch(url);
  const [contatore, setContatore] = useState(0);

  const [bannati, setBannati] = useState(0);

  // con useCallback() la funzione scatterà solo quando verrà aggiornato data
  const addBannati = useCallback(() => {
    setBannati(bannati+1)
  }, [bannati])

  // con useMemo()la funzione scatterà solo quando verrà aggiornato data 
  useMemo(() => {
    trovaMaggiore(data);
  },[data])
  
  return (
    <>
      <div style={{ width: "fit-content", margin: "auto" }}>
        <button
          className="btn btn-info"
          onClick={() => setContatore(contatore + 1)}
        >
          Aggiungi
        </button>
        <h4>{contatore}</h4>
      </div>
      <div style={{ width: "fit-content", margin: "auto" }}>
        {data.map((el) => {
          return <Elenco key={el.id} {...el} addBannati={addBannati} />;
        })}
      </div>
    </>
  );
};
// uso React.memo per controllare lo stato delle nostre props, ovvero se le props non vengono aggiornate lui ritorna il valore precedente
// In questo caso se aumento contatore, il componente Elenco non viene reindirizzato poiche le sue proprietà non sono cambiate
const Elenco = React.memo(({ avatar_url: image, login: name, addBannati }) => {
  console.log('item');
  return (
    <article className="card bg-white my-3 shadow-sm">
      <img
        src={image}
        alt={name}
        className="img my-3"
        style={{ width: "30%", borderRadius: "50%", margin: "auto" }}
      />
      <h4>{name}</h4>
      <button className="btn btn-danger" onClick={addBannati}>Banna</button>
    </article>
  );
});

export default Index;
