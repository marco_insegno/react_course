import React from "react";
import { useState } from "react";

const ShortRender = () => {

  const [parola, setParola] = useState('ciaooooo')

  const esempio = parola || 'Buongiorno'
  const esempio2 = parola && 'Buonaserata'
  const esempio3 = parola === 'ciaooooo' ? 'Sono uguale' : 'Sono diversa'

  const [toggle, setToggle] = useState(false)


  return (
    <div>
      <h2>{esempio}</h2>
      {
        parola && <h6>Lo useState di parola è truthy</h6>
      }

      <hr />

      <h2>{esempio2}</h2>
      {
        esempio2
          ? <h6>Ci sono entrambi i valori</h6>
          : <h6>Uno dei due valori è errato</h6>
      }

      <hr />

      <h2>{esempio3}</h2>

      <hr />
      {toggle
        ? <h4 className="text-success">Vero</h4>
        : <h2 className="text-danger">Falso</h2>
      }

      <button className="btn btn-info" onClick={() => setToggle(!toggle)}>Toggle</button>
    </div>
  );
};

export default ShortRender;
