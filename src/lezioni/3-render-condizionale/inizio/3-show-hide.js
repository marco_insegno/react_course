import React, { useEffect, useState } from "react";

const HideorShowComponent = () => {

  const [show, setShow] = useState(false);

  return (
    <div className="p-5">
      <button className="btn btn-danger" onClick={()=> setShow(!show)}>
        {show ? 'Nascondi' : 'Mostra'}
      </button>
      {
        show && <Componente />
      }
      
    </div>
  );
};

const Componente = () => {

  const [contatore, setContatore] = useState(0);

  useEffect(()=> {

    const time = setTimeout(() => {

      setContatore((oldValue) => {
        return( 
          oldValue + 1
        )
      }
      )
      
    }, 1000);

    return () => clearTimeout(time)

  }, [contatore])


  return (
    <h1 className="mt-3">{contatore}</h1>
  )
}

export default HideorShowComponent;
