import React from "react";
import { useState, useEffect } from 'react'
import axios from 'axios'

const url = "https://api.github.com/users/QuincyLarson";

const ConditionalCompining = () => {

  const [isLoading, setIsLoading] = useState(true)
  const [isError, setIsError] = useState(false)
  const [user, setUser] = useState('default user')


  const getData = async () => {

    setIsError(false)

    setIsLoading(true)

    try {
      const response = await axios.get(url)

      setUser(response.data)

    } catch (error) {

      setIsError(true);

    }

    setIsLoading(false)
    
  }

  console.log(user);

  useEffect(() => {

    getData()

  }, [])

  if (isLoading) {
    return (
      <Loading />
    );

  } else if (isError) {
    return (
      <Error />
    );
  }
  return (
    <div className="card d-flex justify-content-center align-items-center shadow p-5 mt-5">
      <a href={user.url} target="_blank" rel="noreferrer">
        <img
          src={user.avatar_url}
          alt={user.login}
          className="rounded-circle"
          style={{
            width: '200px',
            height: '200px'
          }} />
      </a>
      <h2 className="my-1">{user.login}</h2>
      <h6 className="mb-3 fst-italic">{user.location}</h6>
      <small className="text-muted">Followers: {user.followers}</small>
    </div>
  )


};

const Loading = () => {
  return (
    <div>
      <h2>Loading...</h2>
    </div>
  )
}

const Error = () => {
  return (
    <div>
      <h2>There is an error in API Call...</h2>
    </div>
  )
}

export default ConditionalCompining;
