import React, { useState, useContext, createContext } from "react";
import { data } from "../../../data";
// dichiariamo il context 
const AppContext = createContext(null)


const MainComponent = () => {
  const [people, setPeople] = useState(data);

  const removePeople = (id) => setPeople(people.filter((el) => el.id !== id));
  return (

    // tramite il value posso passare oggetti e funzioni
    <AppContext.Provider value={{ people, removePeople }}>
    <div>
      <h3 className="text-uppercase fw-bolder text-danger display-1">UseContext:</h3>
      <h4>Passaggio proprietà e funzioni tra componenti con useContext</h4>
      <Elenco/>
    </div>
    </AppContext.Provider>
  );
};

const Elenco = () => {
  const {people} = useContext(AppContext)

  return (
    <div>
      {people.map((el, index) => {
        return <Persona key={index} {...el}/>;
      })}
    </div>
  );
};

const Persona = ({ id, name}) => {

  const {removePeople} = useContext(AppContext)

  return (
    <div className="item shadow">
      <h5> {name} </h5>
      <button className="button delete-button" onClick={() => removePeople(id)}>
        {" "}
        x{" "}
      </button>
    </div>
  );
};

export default MainComponent;
