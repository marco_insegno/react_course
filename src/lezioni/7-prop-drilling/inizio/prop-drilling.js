import React from "react";
import { useState } from 'react'

import { data } from '../../../data'

const MainComponent = () => {

  const [people, setPeople] = useState(data)

  const [show, setShow] = useState(true)

  const removePeople = (id) => {
    setPeople(people.filter((el) => el.id !== id))
  }


  return (
    <div>
      <h2>Passaggio di Proprietà a Cascata</h2>

      {show &&
        <>
          <Elenco people={people} removePeople={removePeople} />
        </>
      }
      {people.length > 0 &&
        < button
          className="mt-5 btn btn-success" onClick={() => setShow(!show)}>
          {show
            ? 'Hide'
            : 'Show'
          }
        </button>

      }

      {
        people.length === 0 &&
        <button
          className="mt-5 btn btn-danger" onClick={()=> setPeople(data)} >Reload
        </button>
      }

    </div >
  );
};

const Elenco = ({ people, removePeople }) => {

  return (
    <>
      {
        people.map((el) => {
          return (
            <Persona key={el.id} {...el} removePeople={removePeople} />
          )
        })
      }
    </>
  )
}

const Persona = ({ name, id, removePeople }) => {

  return (
    <article className="item shadow">
      <h5>
        {name}
      </h5>
      <button className="button delete-button" onClick={() => removePeople(id)}>
        x
      </button>
    </article>
  )
}

export default MainComponent;
