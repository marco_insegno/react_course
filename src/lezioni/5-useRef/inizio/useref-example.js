import React from "react";

import { useRef } from "react";

const RefExample = () => {

  const reference = useRef(null)

  console.log(reference.current);


  const handleScroll = () => {

    if(!reference || !reference.current){

      return console.log('manca referenza');

    } else{

      reference.current.scrollIntoView({behavior: "smooth", block: "center" , inline: "nearest"})

    }
  }

  return (
    <>
      <h1>Use Ref</h1>
      <div
        className="my-5 mx-auto"
        style={{
          height: "100vh",
        }}
      >
        <button className="btn btn-info" onClick={handleScroll}>Scroll</button>
      </div>

      <div
        style={{
          height: "100vh",
        }}
      ></div>

      <h2 ref={reference}>Testo Testo</h2>
      <div
        style={{
          height: "30vh",
        }}
      ></div>
    </>
  );
};

export default RefExample;
